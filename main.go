package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"

	"github.com/gossie/router"
)

type price struct {
	ArticleId string `json:"articleId"`
	Price     int    `json:"price"`
}

var pricesByArticleId map[string]price = map[string]price{
	"4711": {"4711", 12300},
	"4712": {"4712", 500075},
	"4713": {"4713", 750099},
}

func readJsonIfPresent() {
	content, err := os.ReadFile("/usr/local/price-service/data.json")
	if err == nil {
		var prices []price
		err := json.Unmarshal(content, &prices)
		if err == nil {
			clear(pricesByArticleId)
			log.Default().Println("Importing", len(prices), "prices")
			for i := range prices {
				p := prices[i]
				pricesByArticleId[p.ArticleId] = p
				log.Default().Println(p, "was imported")
			}
		} else {
			log.Default().Println("JSON could not be parsed, default prices are used:", err.Error())
		}
	} else {
		log.Default().Println("No data.json present, default prices are used")
	}
}

func healthy() bool {
	_, err := os.ReadFile("/usr/local/price-service/unhealthy.txt")
	if err == nil {
		log.Default().Println("unhealthy.txt is present => service is unhealthy")
		return false
	}
	log.Default().Println("unhealthy.txt is not present => service is healthy")
	return true
}

func main() {
	readJsonIfPresent()

	httpRouter := router.New()

	httpRouter.Use(func(hh router.HttpHandler) router.HttpHandler {
		return func(w http.ResponseWriter, r *http.Request, ctx router.Context) {
			w.Header().Add("Content-Type", "application/json")
			hh(w, r, ctx)
		}
	})

	httpRouter.Get("/prices", func(w http.ResponseWriter, r *http.Request, ctx router.Context) {
		log.Default().Println("Retrieve all prices")
		prices := make([]price, 0, len(pricesByArticleId))
		for _, v := range pricesByArticleId {
			prices = append(prices, v)
		}

		j, _ := json.Marshal(prices)
		w.Write(j)
	})

	httpRouter.Get("/prices/:articleId", func(w http.ResponseWriter, r *http.Request, ctx router.Context) {
		articleId := ctx.PathParameter("articleId")
		log.Default().Println("Retrieve price for article with ID", articleId)
		p, present := pricesByArticleId[articleId]
		if !present {
			http.Error(w, "Article with ID "+articleId+" not found", http.StatusNotFound)
			return
		}

		j, _ := json.Marshal(&p)
		w.Write(j)
	})

	httpRouter.Get("/health", func(w http.ResponseWriter, r *http.Request, ctx router.Context) {
		if len(pricesByArticleId) == 0 {
			log.Default().Println("No prices present => service is unhealthy")
			http.Error(w, "unhealthy", http.StatusInternalServerError)
			return
		}

		if !healthy() {
			http.Error(w, "unhealthy", http.StatusInternalServerError)
			return
		}

		w.WriteHeader(200)
		w.Write([]byte("healthy"))
	})

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	log.Default().Println("running on PORT", port)
	log.Fatal(http.ListenAndServe(":"+port, httpRouter))
}
